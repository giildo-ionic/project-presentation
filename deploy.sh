#!/bin/bash

RASP_IP="giildo@82.64.213.115"
PROJECT_DIR="/apps/ionic"

echo "👷 Build the prod version"
npm run build

echo "🗑 Delete the old directory"
ssh $RASP_IP rm -rf $PROJECT_DIR

echo "🚧 Create the new directory"
ssh $RASP_IP mkdir $PROJECT_DIR

echo "📦 Paste the new files"
scp -r dist/* $RASP_IP:$PROJECT_DIR

echo "🎉 Project deployed"
