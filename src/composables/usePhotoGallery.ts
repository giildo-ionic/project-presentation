import {ref, onMounted, Ref} from 'vue'
import {CameraPhoto, CameraResultType, CameraSource, Capacitor, FilesystemDirectory, Plugins} from "@capacitor/core";
import {isPlatform} from "@ionic/vue";

export interface Photo {
    filepath: string;
    webviewPath?: string;
}

const convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader
    reader.onerror = reject
    reader.onload = () => {
        resolve(reader.result)
    }
    reader.readAsDataURL(blob)
})

export function usePhotoGallery() {
    const {Camera, Filesystem, Storage} = Plugins
    const photos = ref<Photo[]>([])
    const photo = ref<Photo>({
        filepath: '',
        webviewPath: '',
    })
    const PHOTO_STORAGE = 'photos'

    const savePicture = async (photo: CameraPhoto, fileName: string): Promise<Photo> => {
        let base64Data: string;

        if (isPlatform('hybrid')) {
            const file = await Filesystem.readFile({
                path: photo.path!
            })
            base64Data = file.data
        } else {
            const response = await fetch(photo.webPath!)
            const blob = await response.blob()
            base64Data = await convertBlobToBase64(blob) as string
        }

        const savedFile = await Filesystem.writeFile({
            path: fileName,
            data: base64Data,
            directory: FilesystemDirectory.Data
        })

        if (isPlatform('hybrid')) {
            return {
                filepath: savedFile.uri,
                webviewPath: Capacitor.convertFileSrc(savedFile.uri)
            }
        } else {
            return {
                filepath: fileName,
                webviewPath: photo.webPath
            }
        }
    }

    const cachePhotos = async (photo: Ref<Photo>) => {
        const photoList = await Storage.get({key: PHOTO_STORAGE})
        const photos = photoList.value ? JSON.parse(photoList.value) : []

        return Storage.set({
            key: PHOTO_STORAGE,
            value: JSON.stringify([photo.value, ...photos])
        })
    }

    const takePhoto = async () => {
        return Camera.getPhoto({
            resultType: CameraResultType.Uri,
            source: CameraSource.Camera,
            quality: 100
        })
            .then(async cameraPhoto => {
                const fileName = new Date().getTime() + '.jpeg'
                photo.value = await savePicture(cameraPhoto, fileName)

                await cachePhotos(photo)
            })
            .catch(error => {
                console.log('no photo')
                console.log(error)
            })
    }

    const loadSaved = async () => {
        const photoList = await Storage.get({key: PHOTO_STORAGE})
        const photosInStorage = photoList.value ? JSON.parse(photoList.value) : []

        if (!isPlatform('hybrid')) {
            for (const photo of photosInStorage) {
                const file = await Filesystem.readFile({
                    path: photo.filepath,
                    directory: FilesystemDirectory.Data
                })
                photo.webviewPath = `data:image/jpeg;base64,${file.data}`
            }
        }

        photos.value = photosInStorage
    }

    onMounted(loadSaved)

    return {
        photo,
        photos,
        takePhoto
    }
}
